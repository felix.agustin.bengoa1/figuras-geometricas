﻿namespace AplicacionFiguras
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            LblCuadradoDatos = new Label();
            TxtCuadradoInfo = new TextBox();
            BtnCrearCuadrado = new Button();
            TxtCrearCirculo = new TextBox();
            LblCirculoDatos = new Label();
            BtnCrearCirculo = new Button();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            BtnCrearTriangulo = new Button();
            TxtTrianguloLado1 = new TextBox();
            TxtTrianguloLado2 = new TextBox();
            TxtTrianguloBase = new TextBox();
            LblLadoPoligono = new Label();
            LblCantidadLadosPoligono = new Label();
            TxtLadoPoligono = new TextBox();
            TxtCantidadPoligono = new TextBox();
            BtnCrearPoligono = new Button();
            SuspendLayout();
            // 
            // LblCuadradoDatos
            // 
            LblCuadradoDatos.AutoSize = true;
            LblCuadradoDatos.Location = new Point(63, 88);
            LblCuadradoDatos.Name = "LblCuadradoDatos";
            LblCuadradoDatos.Size = new Size(168, 20);
            LblCuadradoDatos.TabIndex = 0;
            LblCuadradoDatos.Text = "Ingresar Lado Cuadrado";
            // 
            // TxtCuadradoInfo
            // 
            TxtCuadradoInfo.Location = new Point(63, 124);
            TxtCuadradoInfo.Name = "TxtCuadradoInfo";
            TxtCuadradoInfo.Size = new Size(168, 27);
            TxtCuadradoInfo.TabIndex = 1;
            // 
            // BtnCrearCuadrado
            // 
            BtnCrearCuadrado.Location = new Point(63, 166);
            BtnCrearCuadrado.Name = "BtnCrearCuadrado";
            BtnCrearCuadrado.Size = new Size(168, 29);
            BtnCrearCuadrado.TabIndex = 2;
            BtnCrearCuadrado.Text = "Crear cuadrado";
            BtnCrearCuadrado.UseVisualStyleBackColor = true;
            BtnCrearCuadrado.Click += BtnCrearCuadrado_Click;
            // 
            // TxtCrearCirculo
            // 
            TxtCrearCirculo.Location = new Point(266, 124);
            TxtCrearCirculo.Name = "TxtCrearCirculo";
            TxtCrearCirculo.Size = new Size(125, 27);
            TxtCrearCirculo.TabIndex = 3;
            // 
            // LblCirculoDatos
            // 
            LblCirculoDatos.AutoSize = true;
            LblCirculoDatos.Location = new Point(266, 88);
            LblCirculoDatos.Name = "LblCirculoDatos";
            LblCirculoDatos.Size = new Size(105, 20);
            LblCirculoDatos.TabIndex = 4;
            LblCirculoDatos.Text = "Ingresar Radio";
            // 
            // BtnCrearCirculo
            // 
            BtnCrearCirculo.Location = new Point(266, 167);
            BtnCrearCirculo.Name = "BtnCrearCirculo";
            BtnCrearCirculo.Size = new Size(125, 29);
            BtnCrearCirculo.TabIndex = 5;
            BtnCrearCirculo.Text = "Crear circulo";
            BtnCrearCirculo.UseVisualStyleBackColor = true;
            BtnCrearCirculo.Click += BtnCrearCirculo_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(429, 91);
            label1.Name = "label1";
            label1.Size = new Size(54, 20);
            label1.TabIndex = 6;
            label1.Text = "Lado 1";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(429, 120);
            label2.Name = "label2";
            label2.Size = new Size(54, 20);
            label2.TabIndex = 7;
            label2.Text = "Lado 2";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(429, 153);
            label3.Name = "label3";
            label3.Size = new Size(40, 20);
            label3.TabIndex = 8;
            label3.Text = "Base";
            // 
            // BtnCrearTriangulo
            // 
            BtnCrearTriangulo.Location = new Point(485, 183);
            BtnCrearTriangulo.Name = "BtnCrearTriangulo";
            BtnCrearTriangulo.Size = new Size(126, 29);
            BtnCrearTriangulo.TabIndex = 9;
            BtnCrearTriangulo.Text = "Crear Triangulo";
            BtnCrearTriangulo.UseVisualStyleBackColor = true;
            BtnCrearTriangulo.Click += BtnCrearTriangulo_Click;
            // 
            // TxtTrianguloLado1
            // 
            TxtTrianguloLado1.Location = new Point(486, 84);
            TxtTrianguloLado1.Name = "TxtTrianguloLado1";
            TxtTrianguloLado1.Size = new Size(125, 27);
            TxtTrianguloLado1.TabIndex = 10;
            TxtTrianguloLado1.TextChanged += textBox1_TextChanged;
            // 
            // TxtTrianguloLado2
            // 
            TxtTrianguloLado2.Location = new Point(486, 117);
            TxtTrianguloLado2.Name = "TxtTrianguloLado2";
            TxtTrianguloLado2.Size = new Size(125, 27);
            TxtTrianguloLado2.TabIndex = 11;
            // 
            // TxtTrianguloBase
            // 
            TxtTrianguloBase.Location = new Point(485, 150);
            TxtTrianguloBase.Name = "TxtTrianguloBase";
            TxtTrianguloBase.Size = new Size(125, 27);
            TxtTrianguloBase.TabIndex = 12;
            // 
            // LblLadoPoligono
            // 
            LblLadoPoligono.AutoSize = true;
            LblLadoPoligono.Location = new Point(657, 91);
            LblLadoPoligono.Name = "LblLadoPoligono";
            LblLadoPoligono.Size = new Size(42, 20);
            LblLadoPoligono.TabIndex = 13;
            LblLadoPoligono.Text = "Lado";
            // 
            // LblCantidadLadosPoligono
            // 
            LblCantidadLadosPoligono.AutoSize = true;
            LblCantidadLadosPoligono.Location = new Point(657, 129);
            LblCantidadLadosPoligono.Name = "LblCantidadLadosPoligono";
            LblCantidadLadosPoligono.Size = new Size(73, 20);
            LblCantidadLadosPoligono.TabIndex = 14;
            LblCantidadLadosPoligono.Text = "Cantidad ";
            // 
            // TxtLadoPoligono
            // 
            TxtLadoPoligono.Location = new Point(736, 88);
            TxtLadoPoligono.Name = "TxtLadoPoligono";
            TxtLadoPoligono.Size = new Size(125, 27);
            TxtLadoPoligono.TabIndex = 15;
            // 
            // TxtCantidadPoligono
            // 
            TxtCantidadPoligono.Location = new Point(736, 126);
            TxtCantidadPoligono.Name = "TxtCantidadPoligono";
            TxtCantidadPoligono.Size = new Size(125, 27);
            TxtCantidadPoligono.TabIndex = 16;
            // 
            // BtnCrearPoligono
            // 
            BtnCrearPoligono.Location = new Point(736, 167);
            BtnCrearPoligono.Name = "BtnCrearPoligono";
            BtnCrearPoligono.Size = new Size(123, 53);
            BtnCrearPoligono.TabIndex = 17;
            BtnCrearPoligono.Text = "Crear Poligono Regular";
            BtnCrearPoligono.UseVisualStyleBackColor = true;
            BtnCrearPoligono.Click += button1_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(892, 450);
            Controls.Add(BtnCrearPoligono);
            Controls.Add(TxtCantidadPoligono);
            Controls.Add(TxtLadoPoligono);
            Controls.Add(LblCantidadLadosPoligono);
            Controls.Add(LblLadoPoligono);
            Controls.Add(TxtTrianguloBase);
            Controls.Add(TxtTrianguloLado2);
            Controls.Add(TxtTrianguloLado1);
            Controls.Add(BtnCrearTriangulo);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(BtnCrearCirculo);
            Controls.Add(LblCirculoDatos);
            Controls.Add(TxtCrearCirculo);
            Controls.Add(BtnCrearCuadrado);
            Controls.Add(TxtCuadradoInfo);
            Controls.Add(LblCuadradoDatos);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label LblCuadradoDatos;
        private TextBox TxtCuadradoInfo;
        private Button BtnCrearCuadrado;
        private TextBox TxtCrearCirculo;
        private Label LblCirculoDatos;
        private Button BtnCrearCirculo;
        private Label label1;
        private Label label2;
        private Label label3;
        private Button BtnCrearTriangulo;
        private TextBox TxtTrianguloLado1;
        private TextBox TxtTrianguloLado2;
        private TextBox TxtTrianguloBase;
        private Label LblLadoPoligono;
        private Label LblCantidadLadosPoligono;
        private TextBox TxtLadoPoligono;
        private TextBox TxtCantidadPoligono;
        private Button BtnCrearPoligono;
    }
}
