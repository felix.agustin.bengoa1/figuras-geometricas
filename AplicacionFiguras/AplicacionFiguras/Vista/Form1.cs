using AplicacionFiguras.Controlador;
using AplicacionFiguras.Modelo;

namespace AplicacionFiguras
{
    public partial class Form1 : Form
    {
        private ControllerFiguras Controlador;
        public Form1()
        {
            InitializeComponent();
            Controlador = new ControllerFiguras();
        }

        private void BtnCrearCuadrado_Click(object sender, EventArgs e)
        {
            double lado;
            bool esNumero = double.TryParse(TxtCuadradoInfo.Text, out lado);

            if (!esNumero)
            {
                MessageBox.Show("Ingrese un numero valido para el lado");
                return;
            }

            Controlador.CrearCuadrado(lado);

            var figuras = Controlador.ObtenerFiguras();
            var ultimaFigura = figuras[figuras.Count - 1];

            if (ultimaFigura != null)
            {
                double perimetro = ultimaFigura.CalcularPerimetro();
                double superficie = ultimaFigura.CalcularArea();

                MessageBox.Show($"Per�metro: {perimetro:F2}, �rea: {superficie:F2}");
            }
            else
            {
                MessageBox.Show("Error al obtener el �ltimo Cuadrado.");
            }
        }

        private void BtnCrearCirculo_Click(object sender, EventArgs e)
        {
            double radio;
            bool esNumero = double.TryParse(TxtCrearCirculo.Text, out radio);

            if (!esNumero)
            {
                MessageBox.Show("Ingrese un numero valido para el radio");
                return;
            }

            Controlador.CrearCirculo(radio);
            var figuras = Controlador.ObtenerFiguras();
            var ultimaFigura = figuras[figuras.Count - 1];

            if (ultimaFigura != null)
            {
                double perimetro = ultimaFigura.CalcularPerimetro();
                double superficie = ultimaFigura.CalcularArea();

                MessageBox.Show($"Per�metro: {perimetro:F2}, �rea: {superficie:F2}");
            }
            else
            {
                MessageBox.Show("Error al obtener el �ltimo circulo.");
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e) { }

        private void BtnCrearTriangulo_Click(object sender, EventArgs e)
        {
            double lado1, lado2, baseLado;
            bool esNumero1 = double.TryParse(TxtTrianguloLado1.Text, out lado1);
            bool esNumero2 = double.TryParse(TxtTrianguloLado2.Text, out lado2);
            bool esNumeroBase = double.TryParse(TxtTrianguloBase.Text, out baseLado);

            if (!esNumero1 || !esNumero2 || !esNumeroBase)
            {
                MessageBox.Show("Ingrese numeros validos para los lados");
                return;
            }

            if (lado1 + lado2 <= baseLado || lado2 + baseLado <= lado1 || baseLado + lado1 <= lado2)
            {
                MessageBox.Show("Los lados ingresados no forman un tri�ngulo v�lido.");
                return;
            }

            Controlador.CrearTriangulo(lado1, lado2, baseLado);

            var figuras = Controlador.ObtenerFiguras();
            var ultimaFigura = figuras[figuras.Count - 1] as Triangulo;

            if (ultimaFigura != null)
            {
                double perimetro = ultimaFigura.CalcularPerimetro();
                double superficie = ultimaFigura.CalcularArea();
                double altura = ultimaFigura.CalcularAltura();

                MessageBox.Show($"Per�metro: {perimetro:F2}, �rea: {superficie:F2}, Altura: {altura:F2}");
            }
            else
            {
                MessageBox.Show("Error al obtener el �ltimo tri�ngulo.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double lado;
            int cantidadLados;
            bool esNumero = double.TryParse(TxtLadoPoligono.Text, out lado);
            bool esNumero2 = int.TryParse(TxtCantidadPoligono.Text, out cantidadLados);

            if (!esNumero)
            {
                MessageBox.Show("ingrese numeros validos para los lados");
                return;
            }
            else if (!esNumero2)
            {
                MessageBox.Show("ingrese numeros validos para la cantidad de lados");
                return;
            }

            if (cantidadLados < 3)
            {
                MessageBox.Show("un poligono debe tener almenos 3 lados");
                return;
            }

            Controlador.CrearPoligonoRegular(lado, cantidadLados);

            var figuras = Controlador.ObtenerFiguras();
            var ultimaFigura = figuras[figuras.Count - 1];

            double perimetro = ultimaFigura.CalcularPerimetro();
            double area = ultimaFigura.CalcularArea();

            //string message = $"Perimetro: {perimetro:F2}, Area: {area:F2}";
            //MessageBox.Show(message);
            //MessageBox.Show($"perimetro:{ultimaFigura.CalcularPerimetro()}, Area:{ultimaFigura.CalcularArea()}");
            MessageBox.Show($"Perimetro: {perimetro:F2}, Area: {area:F2}");
            
        }
    }
}
