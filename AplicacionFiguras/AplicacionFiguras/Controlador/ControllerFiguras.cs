﻿using AplicacionFiguras.Interfaces;
using AplicacionFiguras.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionFiguras.Controlador
{
    internal class ControllerFiguras
    {
        private List<IFigura> figuras;

        public ControllerFiguras()
        {
            figuras = new List<IFigura>();
        }

        public void CrearCuadrado(double lado)
        {
            Cuadrado CuadradNuevo = new Cuadrado(lado);
            figuras.Add(CuadradNuevo);
        }

        public void CrearCirculo(double radio)
        {
            Circulo CirculoNuevo = new Circulo(radio);
            figuras.Add(CirculoNuevo);
        }

        public void CrearTriangulo(double lado1, double lado2, double baseLado)
        {
            Triangulo TrianguloNuevo = new Triangulo(lado1, lado2, baseLado);
            figuras.Add(TrianguloNuevo);
        }

        public void CrearPoligonoRegular(double lado, int cantidadLados)
        {
            PoligonoRegular poligonoRegular = new PoligonoRegular(lado, cantidadLados);
            figuras.Add(poligonoRegular);
        }

        public List<IFigura> ObtenerFiguras()
        {
            return figuras;
        }


    }
}
