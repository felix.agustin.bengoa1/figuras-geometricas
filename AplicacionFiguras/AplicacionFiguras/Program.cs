using AplicacionFiguras.Controlador;
using AplicacionFiguras.Interfaces;

namespace AplicacionFiguras
{
    internal static class Program
    {
        [STAThread]
        static void Main()
        {
            ApplicationConfiguration.Initialize();
            Application.Run(new Form1());
        }
    }
}