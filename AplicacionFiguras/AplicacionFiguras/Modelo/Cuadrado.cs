﻿using AplicacionFiguras.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionFiguras.Modelo
{
    internal class Cuadrado : IFigura
    {
        public double Lado {  get; set; }
        public Cuadrado(double lado) {
            Lado = lado;
        }
        public double CalcularPerimetro()
        {
            return Lado * 4;
        }
        public double CalcularArea()
        {
            return Lado * Lado;
        }
    }
}
