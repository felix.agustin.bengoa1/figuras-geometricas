﻿using AplicacionFiguras.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionFiguras.Modelo
{
    internal class Triangulo : IFigura
    {
        double Lado1 {  get; set; }
        double Lado2 { get; set; }
        double Base {  get; set; }

        public Triangulo(double lado1, double lado2, double baseLado) { 
            Lado1 = lado1;
            Lado2 = lado2;
            Base = baseLado;
        }

        public double CalcularPerimetro()
        {
            return Lado1 + Lado2 + Base;
        }

        public double CalcularArea() {

            double s = CalcularPerimetro() / 2;
            return Math.Sqrt(s * (s - Lado1) * (s - Lado2) * (s - Base));
        }

        public double CalcularAltura()
        {
            double area = CalcularArea();
            return (2 * area) / Base;
        }
    }
}
