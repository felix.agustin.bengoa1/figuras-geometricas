﻿using AplicacionFiguras.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionFiguras.Modelo
{
    internal class Circulo : IFigura
    {
        public double Radio { get; set; }

        public Circulo(double radio) { 
            Radio = radio;
        }

        public double CalcularPerimetro()
        {
            return ((Radio * 2) * double.Pi);
        }

        public double CalcularArea()
        {
            return ((Radio * Radio) * double.Pi);
        }
    }
}
