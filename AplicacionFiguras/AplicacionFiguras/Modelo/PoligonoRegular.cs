﻿using AplicacionFiguras.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionFiguras.Modelo
{
    internal class PoligonoRegular : IFigura
    {
        public double Lado { get; set; }
        public int CantidadLados { get; set; }

        public PoligonoRegular(double lado, int cantidadLados)
        {
            Lado = lado;
            CantidadLados = cantidadLados;
        }

        public double CalcularPerimetro()
        {
            return Lado * CantidadLados;
        }

        public double CalcularArea()
        {
            double perimetro = CalcularPerimetro();

            double angulo = (Math.PI * 2) / CantidadLados;

            double apotema = Lado / (2 * Math.Tan(angulo / 2));

            return (perimetro * apotema) / 2;
        }
    }
}
