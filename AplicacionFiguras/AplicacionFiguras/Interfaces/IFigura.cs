﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionFiguras.Interfaces
{
    internal interface IFigura
    {
        double CalcularPerimetro();
        double CalcularArea();
    }
}
